# scrapNews

Test

## Installation & Usage

1. Clone project `git clone https://gitlab.com/dinhbinh1610/scrap-news`

2. cd ./scrap-news

3. Build Docker Image `docker build -t scrap-news-image .`

4. Run Docker Container `docker run -it scrap-news-image /bin/bash`

5. Scrapy crawl `scrapy crawl kenh14 -a rss=http://kenh14.vn/home.rss -a category=home -a source=kenh14`

## License

###TODO

- Interval: khoảng thời gian tự động chạy lại, tính theo đơn vị phút. Ví dụ: nếu đưa vào là 15 thì có nghĩa là cứ 15 phút, crawler sẽ tự động chạy để lấy dữ liệu mới

