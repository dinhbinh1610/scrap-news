FROM python:2.7.12

RUN mkdir -p /usr/src/app/scrapNews/

WORKDIR /usr/src/app/scrapNews

ADD . /usr/src/app/scrapNews/

RUN pip install -r requirements.txt

CMD ["python", "scrapy crawl kenh14 -a rss=http://kenh14.vn/home.rss -a category=home -a source=kenh14"]
