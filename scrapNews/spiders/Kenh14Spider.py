# -*- coding: utf8 -*-
import scrapy
from scrapNews.items import ScrapnewsItem

class Kenh14Spider(scrapy.Spider):
    name = "kenh14"

    def __init__(self, rss='', category='unknown', source='unknown', interval=15, *args, **kwargs):
        super(Kenh14Spider, self).__init__(*args, **kwargs)
        self.rss = rss
        self.category = category
        self.source = source
        self.interval = interval

    def start_requests(self):
        urls = [
            self.rss,
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_rss)

    def parse_content(self, response):
        item = response.meta['item']
        content = response.xpath('//*[@class="knc-content"]').extract()[0].encode('utf-8').strip()
        item['content'] = content
        yield item

    def parse_rss(self, response):
        nodes = response.xpath('//rss/channel/item')

        for node in nodes:
            item = ScrapnewsItem()
            item['category'] = self.category
            item['source'] = self.source
            item['title'] = node.xpath('.//title/text()').extract()[0].encode('utf-8').strip()
            item['url'] = node.xpath('.//link/text()').extract()[0].encode('utf-8').strip()
            item['description'] = node.xpath('.//description/text()').extract()[0].encode('utf-8').strip()
            item['id'] = node.xpath('.//guid/text()').extract()[0].encode('utf-8').strip()
            item['update_time'] = node.xpath('.//pubDate/text()').extract()[0].encode('utf-8').strip()
            request = scrapy.Request(
                item['url'],
                callback=self.parse_content
            )
            request.meta['item'] = item
            yield request




