import datetime
import uuid
import json
from bs4 import BeautifulSoup

from scrapNews.database.firebaseConnection import db

class ScrapnewsPipeline(object):
    def process_item(self, item, spider):
        item['image'] = self.findThumnail(item['description'])
        self.saveToDB(item['category'], item)
        print('item', item['title'])
        return item

    def saveToDB(self, category, item):
        key = item['id'] or str(uuid.uuid4())
        db.child(category).child(key).set(json.dumps(item, default=lambda o: o.__dict__))

    def findThumnail(self, description):
        soup = BeautifulSoup(description, 'lxml')
        img = soup.find('img')
        if img:
            link_img = soup.find('img').get('src')
            return link_img


